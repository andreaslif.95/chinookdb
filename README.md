
##Task: SQL
For this task you will use the provided chinook database (link):
(Use this version of the database)
There are many sites that discuss this database:

Here
or here
Write code to open the sqlite version of the database using the org.xerial JDBC driver.

For a given  customer from inside the database:

- [X] Display the customer's full name
- [X] Display that single customer's most popular genre (in the case of a tie, display both).
(The genre that corresponds to the most tracks from invoices associated to that customer)
- [X] Your code should prompt for a customer ID and display the above information for that customer.
- [X] If no customer ID is given, your code must pick a valid random customer from the database.
(hint: You may use args[] for user input.)

BONUS TASK (not graded, for bragging rights):
- [X] Calculate the total sales made between two dates entered by the user.
