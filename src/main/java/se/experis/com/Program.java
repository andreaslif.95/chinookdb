package se.experis.com;

import se.experis.com.Controller.Controller;

public class Program {
    public static void main(String[] args) {
        new Controller().start();
    }

}
