package se.experis.com.exceptions;

/**
 * Used to diferintiate erros.
 */
public class ConnectionException extends Exception{
    public ConnectionException(String message) {
        super(message);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
