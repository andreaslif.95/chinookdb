package se.experis.com.sql;

import se.experis.com.exceptions.ConnectionException;

import java.io.IOException;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;

public class SqlHandler {
    private final static String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";

    /**
     * Extracts the most popular genre(S) for the specific user with the given id.
     * If multiple genres has the same occurence count it will return both within a string.
     * @param id - ID of the customer to search
     * @return - A string containing all favoritegenres.
     * @throws ConnectionException - If failure occurs while connecting or closing the connection.
     * @throws SQLException - If an error occurs while handling sql with the database.
     */
    public static String getMostPopularGenreFromCustomerId(int id) throws ConnectionException, SQLException {
        Connection connection = connect();
       //Fetches all GenreIds and count their occurences, orders them by descending.
        PreparedStatement preparedStatement =
                connection.prepareStatement(
                        "SELECT GenreId, count(GenreId) c " +
                                "FROM Track " +
                                "WHERE TrackId IN (SELECT TrackId " +
                                "FROM InvoiceLine " +
                                "WHERE InvoiceId IN (SELECT InvoiceId " +
                                "FROM Invoice " +
                                "WHERE CustomerId = ?))" +
                                "GROUP BY GenreId " +
                                "ORDER BY count(GenreId) " +
                                "DESC");

        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        int highest = resultSet.getInt("c");
        int lastGenreId = resultSet.getInt("GenreId");
        //Here we loop through all the result and see if there are more than 1 Genre that is considered the favorite.
        //We also format a string so that we can use it within our query.
        String s = "" + lastGenreId + ",";
        while(resultSet.next() ) {
            if(resultSet.getInt("c") == highest ) {
                if(lastGenreId != resultSet.getInt("GenreId")) {
                    lastGenreId = resultSet.getInt("GenreId");
                    s += " " + lastGenreId + ",";
                }
            }
            else {
                s += s.substring(0,s.length()-1);
                break;
            }
        }
        //Here we fetch all of the names of the favorite genres.
        PreparedStatement preparedStatement1 = connection.prepareStatement("SELECT Name FROM Genre WHERE GenreId in ("+s+")");
        resultSet = preparedStatement1.executeQuery();
        String genreNames = "";
        while(resultSet.next()) {
            genreNames += resultSet.getString("Name") + "+";
        }
        connection.close();
        return genreNames.substring(0,genreNames.length()-1);
    }

    /**
     * Get a Customers full name form an id.
     * @param id - Id of the customer
     * @return - String containing the full name
     * @throws ConnectionException - If failure occurs while connecting or closing the connection.
     * @throws SQLException - If an error occurs while handling sql with the database.
     */
    public static String getFullCustomerNameFromId(int id) throws SQLException, ConnectionException {
        Connection connection = connect();

        PreparedStatement preparedStatement =
                connection.prepareStatement("SELECT FirstName, LastName FROM Customer WHERE CustomerId = ? LIMIT 1");
        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        String fullName = resultSet.getString("FirstName") + " " + resultSet.getString("LastName");
        closeConnection(connection);
        return fullName;
    }

    /**
     * Counts the number of names within the customers eg. the total amount of customers and returns a random number between 1 and the total number of customers.
     * @return - an String with the random number between 1 and the total number of customers.
     * @throws ConnectionException - If failure occurs while connecting or closing the connection.
     * @throws SQLException - If an error occurs while handling sql with the database.
     */
    public static String getRandomId() throws SQLException, ConnectionException {
        Connection connection = connect();
        PreparedStatement preparedStatement =
                connection.prepareStatement("SELECT count(firstName) as count FROM Customer LIMIT 1");

        ResultSet resultSet = preparedStatement.executeQuery();
        int amountOfCustomers = resultSet.getInt("count");
        int randomId = (int) (Math.random() * amountOfCustomers) +1;
        closeConnection(connection);
        return "" +randomId;
    }

    /**
     * Gets the total amount of sales of a specific Customer within a time period.
     * @param b - Date which the sales should be after.
     * @param a - Date which the sales should be before.
     * @param id - Id of the customer.
     * @return - An int with the total number of sales.
     * @throws ConnectionException - If failure occurs while connecting or closing the connection.
     * @throws SQLException - If an error occurs while handling sql with the database.
     */
    public static int getSales(Date b, Date a, String id) throws SQLException, ConnectionException {
        Connection connection = connect();
        ResultSet resultSet;
        PreparedStatement preparedStatement;
            preparedStatement =
                    connection.prepareStatement("SELECT InvoiceDate, Total FROM Invoice WHERE CustomerId = ?");
        preparedStatement.setString(1,id);
        resultSet = preparedStatement.executeQuery();
        int total = 0;
        while(resultSet.next()) {
            try {
                //Takes the date of the invoice. Splits it at the space since the database stores it as 2020-10-10 0:00:00 and we only want the actual date not the time.
                Date temp = new SimpleDateFormat("yyyy-MM-dd").parse(resultSet.getString("InvoiceDate").split(" ")[0]);
                //Checks so that it's between the actual dates provided.
                if(temp.after(b) && temp.before(a)) {
                    total += resultSet.getInt("Total");
                }
            } catch (ParseException e) {
                connection.close();
                throw new SQLException();
            }
        }
        connection.close();
        return total;
    }

    /**
     * Tries to connect to the database for 10 times
     * @return - A Connection
     * @throws ConnectionException - If it fails to connect after 10 retries.
     */
    private static Connection connect() throws ConnectionException {
        int connectionTries = 0;
        while(true) {
            try {
                if (connectionTries > 10) {
                    throw new ConnectionException("Failed to connect to the database.");
                }
                return DriverManager.getConnection(URL);
            } catch (SQLException throwables) {
                connectionTries++;
            }
        }
    }

    /**
     * Closes the connection
     * If the closing failes it tries 10 times in total.
     * @param con - The connection which should be closed.
     * @throws ConnectionException - If it fails to connect after 10 retries.
     */
    private static void closeConnection(Connection con)  throws ConnectionException{
        int closingTries = 0;
        while(true) {
            try {
                if(closingTries > 10){
                    throw new ConnectionException("Failed to close the connection to the database.");
                }
                con.close();
                break;
            }
            catch(SQLException e) {
                closingTries++;
            }
        }
    }
 }
