package se.experis.com.Controller;

import se.experis.com.exceptions.ConnectionException;
import se.experis.com.sql.SqlHandler;
import se.experis.com.view.View;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Controller {
    public void start() {
        while(true) {
            String input = View.startMessage();
            if (input.contains("1")) {
                displayCustomerFlow();
            } else if (input.contains("2")) {
                totalSalesFlow();
            } else if (input.contains("3")) {
                break;
            } else {
                View.invalidInput();
            }
        }
    }

    /**
     * Handels the customer flow.
     * Will ask the user for the id of the customer he wants to view.
     * If a invalid id is inputted a random will be generated.
     * Incase of error the user will be asked if they want to try again.
     */
    private void displayCustomerFlow() {
        String input = View.userProvideId();
        if(!input.matches("[0-9]+")) {
            View.invalidInputGeneratingRandom();
            while(true) {
                try {
                    input = SqlHandler.getRandomId();
                    break;
                } catch(Exception e) {
                    if(!handleErrors(e)) {
                        return;
                    }
                }
            }
        }
        while(true) {
            try {
                View.printCustomerInfo(SqlHandler.getFullCustomerNameFromId(Integer.parseInt(input)), SqlHandler.getMostPopularGenreFromCustomerId(Integer.parseInt(input)));
                break;
            } catch(Exception e) {
                if(!handleErrors(e)) {
                    return;
                }
            }
        }

    }

    /**
     * Handles the flow of the sales print.
     * Will get User input in the form of [id, date, date] and use that to call SqlHandler to get the total sales.
     * In case of errors handleError is called or it's handeled and lets the user chose wether to continue or not.
     */
    private void totalSalesFlow() {
        while(true) {
            String input = View.userProvideIdAndDates();
            String[] inputs = input.split(" ");
            if(!inputs[0].matches("[0-9]+")) {
                View.invalidInput();
            }
            else {
                try {
                    //Parsers the dates in for the given format. If the input is in different format a exception is thrown.
                    Date b = new SimpleDateFormat("yyyy-MM-dd").parse(inputs[1]);
                    Date a = new SimpleDateFormat("yyyy-MM-dd").parse(inputs[2]);
                    int sales;
                    while (true) {
                        try {
                            sales = SqlHandler.getSales(b, a, inputs[0]);
                            break;
                        } catch(Exception e) {
                            if(!handleErrors(e)) {
                                return;
                            }
                        }
                    }
                    View.printTotalSales(sales);
                    break;
                } catch (ParseException e) {
                    View.invalidDatesInput();
                    if (!View.userTryAgain().toLowerCase().contains("y")) {
                        break;
                    }
                }
            }
        }
    }

    /**
     * Used to reduce code duplication. Will let the user chose wether to continue or not.
     * @param e - The exception
     * @return - The user choice of continueing or not.
     */
    private boolean handleErrors(Exception e) {
        if(e instanceof ConnectionException) {
            System.out.println(e.getMessage());
            return View.userTryAgain().toLowerCase().contains("y");
        }else if(e instanceof SQLException) {
            // IF resultSet closed is within th error text, the resource is non existent.
            if(e.getMessage().contains("ResultSet closed")) {
                View.resourceNotFound();
                return false;
            }
            else {
                View.resourceFailedToLoad();
                return View.userTryAgain().toLowerCase().contains("y");
            }
        }
        return true;
    }

}
