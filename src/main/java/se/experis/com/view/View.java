package se.experis.com.view;

import java.sql.SQLOutput;
import java.util.Scanner;

/**
 * Used for all the prints within the application.
 */
public class View {
    /**
     * Takes input for the menu choice.
     * @return - The input.
     */
    public static String startMessage() {
        System.out.println("What would you like to do? " +
                "\n\t1.Display customer name and most popular genre by id. " +
                "\n\t2.Display total sales between two dates." +
                "\n\t3.Exit");
        return new Scanner(System.in).nextLine();
    }
    public static void invalidInput() {
        System.out.println("Invalid input, please try again.");
    }
    public static void invalidInputGeneratingRandom() {
        System.out.println("Invalid input, generating random id.");
    }

    /**
     * Takes input for the customer id.
     * @return - The input.
     */
    public static String userProvideId() {
        System.out.println("Please provide id you would like to search for:");
        return new Scanner(System.in).nextLine();
    }
    public static void invalidInputCustomer() {
        System.out.println("Invalid input, providing random customer.");
    }
    public static void printCustomerInfo(String name, String genres) {
        System.out.println("Fullname: " + name + "\nFavoite Genre(s): " + genres);
    }
    public static void printTotalSales(int sales) {
        System.out.println("Total number of sales in the given period : " + sales);
    }

    /**
     * Takes input for id and dates.
     * @return - Returns the input
     */
    public static String userProvideIdAndDates() {
        System.out.println("Please provide the customer id followed the dates in the format yyyy-mm-dd seperated by a space, starting with the oldest date: [id, date, date]");
        return new Scanner(System.in).nextLine();
    }
    public static String userTryAgain() {
        System.out.println("Would you like to try again? (Y/N)");
        return new Scanner(System.in).nextLine();
    }
    public static void invalidDatesInput() {
        System.out.println("You provided the dates in a bad format.");
    }
    public static void resourceNotFound() {
        System.out.println("The requested resource did not exist.");
    }
    public static void resourceFailedToLoad() {
        System.out.println("The requested resource failed to load.");
    }
}
